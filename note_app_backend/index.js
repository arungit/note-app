const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

const dbConfig = require('./config/database.config');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

mongoose.connect(dbConfig.url,{
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
}).then(() => {
    console.log("Connected to database");
}).catch(err => {
    console.log('Could not connect to database', err);
    process.exit();
});
// Parse requests of content-type
app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());
app.use(cors());
require('./src/routes/note.routes')(app);

app.listen(4000, () =>{
    console.log("Server is listening on port 4000");
}); 
