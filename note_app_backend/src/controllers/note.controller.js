const Note = require('../models/note.model');
const User = require('../models/note.user');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const Joi = require('joi');
exports.create = (req, res) => {
    if(!req.body.content) {
        return res.status(400).send({
            message: "Note content cannot be empty"
        });
    }

    //Create a note
    const note = new Note({
        title: req.body.title,
        content: req.body.content
    });

    note.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message
        });
    });
};

exports.findAll = (req, res) => {
    Note.find()
    .then(notes => {
        res.send(notes);
    }).catch(err =>{
        res.status(500).send({
            message: err.message || "Some error occured"
        });
    });

};

exports.findOne = (req, res) => {
    Note.findById(req.params.noteId)
    .then(note => {
        if(!note) {
            return res.status(404).send({
                message: "Note not found with Id " + req.params.noteId
            });
        }
        res.send(note);
    }).catch(err => {
        if(err.kind === 'objectId') {
            return res.status(404).send({
                message: "Note not found with Id " + req.params.noteId
            });
        }
        return res.status(500).send({
            message: "Error retrieving note with Id " + req.params.noteId
        });
    });
};

exports.update = (req, res) => {

    Note.findByIdAndUpdate(req.params.noteId, {
        title: req.body.title || "Untitled Note",
        content: req.body.content
    }, {new: true})
    .then(note => {
        if(!note){
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });
        }
        res.send(note);
    }).catch(err => {
        if(err.kind === 'objectId') {
            return res.status(404).send({
                message: "Note not found with Id" + req.params.noteId
            });
        }
        return res.status(500).send({
            message: "Error updating Note with Id" + req.params.noteId
        });
    });
}

exports.remove = (req, res) => {
    Note.findByIdAndRemove(req.params.noteId)
    .then(note => {
        if(!note){
            return res.status(404).send({
                message: "Note not found with Id" + req.params.noteId
            });
        }
        res.send("Note Removed");
    }).catch(err => {
        if(err.kind === 'object.id') {
            return res.status(404).send({
                message: "Note not found with Id " + req.params.noteId 
            });
        }
        return res.status(500).send({
            message: "Error removing note with ID " + req.params.noteId
        });
    })
}

exports.userRoute = async (req, res) => {
    console.log(req.body);
    const schema ={
        username:Joi.string().min(5).max(50).required(),
        email:Joi.string().min(5).max(255).required().email(),
        password:Joi.string().min(5).max(255).required()
    };
    const {error} =  Joi.validate(req.body , schema);
    // console.log(error.details[0].message);
    if(error){
         res.status(404).send(error.details[0].message);
    }
    // const email = req.body.email
    const user = await User.findOne({'email':req.body.email}).exec();
    console.log(user);
    if(user === null){
        console.log('error occured')
        res.status(400).send("Error: No user found with email  " + req.body.email);
    } 
    if(!bcrypt.compareSync(req.body.password, user.password)){
        res.status(400).send("The password is invalid")
    }else {
        res.status(200).send({validate: true});
    }
}

exports.registerUser = async (req, res) => {
    const schema ={
        username:Joi.string().min(5).max(50).required(),
        email:Joi.string().min(5).max(255).required().email(),
        password:Joi.string().min(5).max(255).required()
    };
    const {error} = Joi.validate(req.body , schema);
    if(error){
         res.status(404).send(error.details[0].message);        
    }
    req.body.password = bcrypt.hashSync(req.body.password, 10)
    console.log(req.body.password);
    const user = new User ({
        name: req.body.username,
        email: req.body.email,
        password: req.body.password
    })
    await user.save()
    .then(data =>{
        res.send(data);
    })
}

exports.getUsers = (req, res) => {
    User.find()
    .then(users => {
        res.send(users);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occured"
        });
    });
};


