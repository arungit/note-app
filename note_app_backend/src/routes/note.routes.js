module.exports = (app) => {
    const notes = require('../controllers/note.controller')

    // Create Notes
    app.post('/api/notes', notes.create);

    // Retrieve Notes
    app.get('/api/notes', notes.findAll);

    // Finding Notes by ID
    app.get('/api/notes/:noteId', notes.findOne);

    // Updating a Note by ID
    app.post('/api/notes/:noteId', notes.update);

    // Deleting a note by Id
    app.post('/api/notes/remove/:noteId', notes.remove);

    // Validate User to respective pages 
    app.post('/api/note/userRoute', notes.userRoute);

    // Register a User
    app.post('/api/note/registerUser', notes.registerUser ); 
    
    //get all users
    app.get('/api/note/getAllUsers', notes.getUsers);
}
