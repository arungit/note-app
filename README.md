# Note App

A basic Note-Application Using MEAN Stack Technology

#### Reactjs Login page  
![Login Page](loginpage.png)

##### Enter Credentials to Register to the Note-App
![Enter Credentials](enterCredentials.png)

##### On Successfull Registration 
![Successfull Registration](userRegistrationSuccessfull.png)

#### Using Mongodb compass view registered user

![View Registered User](registeredUserSuccesfullyStored.png)


#### Install this application 

Clone the repository to your local machine using
`git clone https://gitlab.com/arungit/note-app.git `


> Backend Setup
Change directory to note-app
`cd note-app`
Change directory to note-app-backend 
`cd note-app-backend`
Now install the dependencies 
`npm install`
This will automatically install the dependencies for the backend code


> Frontend Setup
Change directory from note-app-backend to note-app-frontend
`cd ../note-app-frontend`
Now install the dependencies 
`npm install`
This will automatically install the dependencies for the frontend code

> Database Setup using docker
Pull the docker image for mongodb using the command
`docker pull mongo`
Start the mongodb container using the command
` docker run --name note-app-db -p 27017:27017 mongo -d`
Now database is ready to function

> This Project is still in its initial phase . 