import React,{Component} from 'react';
import { withRouter } from 'react-router'
import { BrowserRouter, Route, Link} from 'react-router-dom';
import '../note/note.css';

const routes = [
    {
        path: '/Listallnotes',
        exact: true,
        sidebar: () => <div>List All Notes !</div>,
        main: () => <h2>List all Notes</h2>,
    },
    {
        path: '/update',
        sidebar: () => <div>Update a Note !</div>,
        main: () => <h2>Update a Note</h2>,
    },
    {
        path: '/remove',
        sidebar: () => <div>Remove!</div>,
        main: () => <h2>Remove a Note</h2>,
    }
]
class note extends Component {
    render(){
        return(
            <BrowserRouter>
               <div style={{display: 'flex'}}>
                    <div>
                        <ul>
                            <li><Link to='/Listallnotes'>List All Notes</Link></li>
                            <li><Link to='/update'>Update Note</Link></li>
                            <li><Link to='/remove'>Remove Note</Link></li>
                        </ul>
                    </div>
                    <div></div>
                </div> 

               
            </BrowserRouter>
        )
    }
}

export default withRouter(note);