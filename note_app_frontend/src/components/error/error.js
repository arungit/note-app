import React,{Component} from 'react';
import { withRouter } from 'react-router'
import {Container, Row, Col} from 'react-bootstrap';

class error extends Component {
    render(){
        return(
            <div>
                <Container>
                    <Row>
                        <Col md={4}>
                            <div><h1>You are not authorized</h1></div>
                        </Col>
                        
                    </Row>
                </Container>
            </div>
        )
    }
}

export default withRouter(error);