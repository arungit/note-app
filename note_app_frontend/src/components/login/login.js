import React,{Component} from 'react';
import { withRouter } from 'react-router';
import { Route } from 'react-router-dom'
//import * as bs from '../Bootstrap/bootstrap.min.css';
import {Button} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../login/login.css';
import Axios from 'axios';
import swal from 'sweetalert';
import register from '../register/register';

class login extends Component {
    state={
        // username:'',
        email:'',
        password:'',
        errorMessage:''
    }
    try = () => {
        this.props.history.push('/register');
    }
    // handleUsernameChange = (event) => {
    //     this.setState({
    //         username:event.target.value
    //     })
    // };

    handleEmailChange = (event) =>{
        this.setState({
            email:event.target.value
        })
    };

    handlePasswordChange = (event) => {
        this.setState({
            password:event.target.value
        })
    };
    
    handleUserLogin = (event) => {
        event.preventDefault();
        var hist = this.props.history
        var credentials = {
            // "username": this.state.username,
            "email": this.state.email,
            "password": this.state.password
        }
        Axios.post('http://localhost:4000/api/note/userRoute', credentials, {
            headers:{
                'Content-Type':'application/json'
            }
        })
        .then((response) =>{
            if(response.data.validate === true){
                console.log(response.data);
                swal({
                    icon: 'success',
                    title: 'Success',
                    text: "Successfully Logged in !!"
                })
                hist.push('/note');
            }
        }).catch((error) => {
            console.log(error)
            swal({
                icon: 'error',
                title: 'Oops...',
                text: error.response.data
            })
        })
    };

    // handleUserRegistration = async (event) => {
    //     event.preventDefault();
    //     var credentials = {
    //         "username": this.state.username,
    //         "email": this.state.email,
    //         "password": this.state.password
    //     }
    //     // console.log(credentials.username);
    //     // if(credentials.username && credentials.email && credentials.password === ''){
    //     //     swal({
    //     //         icon: 'error',
    //     //         title: 'Oops...',
    //     //         text: 'Please enter Credentials'
    //     //     })
    //     // }
    //     await Axios.post('http://localhost:4000/api/note/registerUser', credentials, {
    //         headers:{
    //             "Content-Type": "application/json"
    //         }
    //     })
    //     .then((response) =>{
    //         console.log(response);
    //         if(response){
    //             swal({
    //                 icon: 'success',
    //                 title: 'success',
    //                 text: 'User registered with email' 
    //             })
    //         }
            
    //     }).catch((error) => {
    //         console.log(error);
    //         swal({
    //             icon: 'error',
    //             title: 'Oops...',
    //             text: error.response.data
    //         })
    //     })

    // }
    render(){
        return(
            <div className="Login">
                <h1 className="Banner">Note App</h1>
                <br/>
                <form className="Login_form" onSubmit={this.handleUserLogin}>
                    <div>
                        <label>Email:</label>
                        <input type="text" onChange={this.handleEmailChange} value={this.state.email}/>
                    </div>
                    <div>
                        <label>Password:</label>
                        <input type="password" onChange={this.handlePasswordChange} value={this.state.password}/>
                    </div>
                    <br/>

                    <div>
                        <Button variant="dark" type="submit" >Login</Button>
                        <Button variant="dark" type="button" onClick={this.try} >Signup</Button>
                        <Route path="/register" component={register}/>
                    </div>
                    <div className="error">{this.state.errorMessage}</div>
                </form>
            </div>
        )
    }
    
}

export default withRouter(login);
