import React,{Component} from 'react';
import { withRouter } from 'react-router';
import { Route } from 'react-router-dom'
import {Button} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../register/register.css'
import Axios from 'axios';
import swal from 'sweetalert';
import login from '../login/login'
class register extends Component {
    state={
        username:'',
        email:'',
        password:'',
        errorMessage:''
    }
    try = () => {
        this.props.history.push('/');
    }
    handleUsernameChange = (event) => {
        this.setState({
            username:event.target.value
        })
    };

    handleEmailChange = (event) =>{
        this.setState({
            email:event.target.value
        })
    };
    handlePasswordChange = (event) => {
        this.setState({
            password:event.target.value
        })
    };
    handleUserRegistration = async (event) => {
    event.preventDefault();
    var credentials = {
        "username": this.state.username,
        "email": this.state.email,
        "password": this.state.password
    }
    // console.log(credentials.username);
    // if(credentials.username && credentials.email && credentials.password === ''){
    //     swal({
    //         icon: 'error',
    //         title: 'Oops...',
    //         text: 'Please enter Credentials'
    //     })
    // }
    await Axios.post('http://localhost:4000/api/note/registerUser', credentials, {
        headers:{
            "Content-Type": "application/json"
        }
    })
    .then((response) =>{
        console.log(response);
        if(response){
            swal({
                icon: 'success',
                title: 'success',
                text: 'User registered with email' 
            })
        }
        
    }).catch((error) => {
        console.log(error);
        swal({
            icon: 'error',
            title: 'Oops...',
            text: error.response.data
        })
    })

}
    render(){
        return(
 
            <div className="Register">
                <h1 className="Banner">Note App</h1>
                <br/>
                <form className="Register_form" onSubmit={this.handleUserRegistration}>
                    <div>
                        <label>Username:</label>
                        <input type="text" onChange={this.handleUsernameChange} value={this.state.username}/>
                    </div>
                    <div>
                        <label>Email:</label>
                        <input type="text" onChange={this.handleEmailChange} value={this.state.email}/>
                    </div>
                    <div>
                        <label>Password:</label>
                        <input type="password" onChange={this.handlePasswordChange} value={this.state.password}/>
                    </div>
                    <br/>

                    <Button variant="dark" type="button" type="submit"  >Signup</Button>
                    <div>
                        <Button variant="dark" onClick={this.try} >Back to Login Page</Button>
                        <Route path="/" component={login}/>
                    </div>
                    <div className="error">{this.state.errorMessage}</div>
                </form>
            </div>

        )
    }
}

export default withRouter(register);