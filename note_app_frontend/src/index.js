import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter,Route,Switch} from 'react-router-dom';

//Components
import login from './components/login/login';
import note from './components/note/note';
import error from './components/error/error';
import register from './components/register/register'
import 'bootstrap/dist/css/bootstrap.min.css';
const App = () => { 
    return(
        <BrowserRouter>
        <Switch>
            <Route path="/" exact component={login}/>
            <Route path="/note" component={note}/>
            <Route path="/error" component={error}/>
            <Route path="/register" component={register}/>
        </Switch>
        </BrowserRouter>
    )
}

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.register();
